package ru.vlasova.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vlasova.taskmanager.model.entity.Role;

public interface RoleRepository extends JpaRepository<Role, String> {
}
