package ru.vlasova.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.taskmanager.model.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User findByUsername(String username);

}
