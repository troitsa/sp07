package ru.vlasova.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.model.dto.TaskDTO;

import java.util.List;

@FeignClient("task")
public interface TaskClient {

    static TaskClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @PostMapping(value = "/rtask")
    TaskDTO createTask(@RequestHeader("Authorization") String header,
                       TaskDTO task);

    @GetMapping(value = "/rtasks")
    List<TaskDTO> getTasks(@RequestHeader("Authorization") String header);

    @GetMapping(value = "/rtask/{id}")
    TaskDTO getTask(@RequestHeader("Authorization") String header,
                    @PathVariable("id") String id);

    @PutMapping(value = "/rtask/{id}")
    TaskDTO updateTask(@RequestHeader("Authorization") String header,
                       @PathVariable("id") String id, TaskDTO task);

    @DeleteMapping(value = "/rtask/{id}")
    void deleteTask(@RequestHeader("Authorization") String header,
                    @PathVariable("id") String id);

    @GetMapping(value = "/rtasks/search/{keyword}")
    List<TaskDTO> searchTasks(@RequestHeader("Authorization") String header,
                              @PathVariable(name = "keyword") String keyword);

    @GetMapping(value = "/rproject/tasks/{id}")
    List<TaskDTO> getTasksByProject(@RequestHeader("Authorization") String header,
                                    @PathVariable("id") String id);

}
