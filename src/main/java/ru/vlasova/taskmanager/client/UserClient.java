package ru.vlasova.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;

@FeignClient("user")
public interface UserClient {

    static UserClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(UserClient.class, baseUrl);
    }

    @PostMapping(value = "/ruser")
    UserDTO createUser(UserDTO user);

    @GetMapping(value = "/rusers")
    List<UserDTO> getUsers(@RequestHeader("Authorization") String header);

    @GetMapping(value = "/ruser/{id}")
    UserDTO getUser(@RequestHeader("Authorization") String header,
                    @PathVariable(name = "id") String id);

    @DeleteMapping(value = "/ruser/{id}")
    void deleteUser(@RequestHeader("Authorization") String header,
                    @PathVariable(name = "id") String id);

    @PutMapping(value = "/ruser/{id}")
    UserDTO updateUser(@RequestHeader("Authorization") String header,
                       @PathVariable(name = "id") String id, UserDTO user);
}
