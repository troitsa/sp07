package ru.vlasova.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.taskmanager.model.dto.UserDTO;

import java.util.List;

public class UserTest extends AbstractTest {

    @Test
    public void createUser() {
        @NotNull final UserDTO user = newUser("1");
        userClient.createUser(user);
        String auth = login(user.getUsername(), user.getPassword());
        Assert.assertNotNull(userClient.getUser(auth, user.getId()));
        userClient.deleteUser(auth, user.getId());
    }

    @Test
    public void getUser() {
        @NotNull final UserDTO user = newUser("1");
        userClient.createUser(user);
        String auth = login(user.getUsername(), user.getPassword());
        Assert.assertEquals(userClient.getUser(auth, user.getId()).getUsername(), user.getUsername());
        userClient.deleteUser(auth, user.getId());
    }

    @Test
    public void getUsers() {
        @NotNull final UserDTO user = newUser("0");
        @NotNull final UserDTO user1 = newUser("1");
        @NotNull final UserDTO user2 = newUser("2");
        userClient.createUser(user);
        userClient.createUser(user1);
        userClient.createUser(user2);
        String auth = login(user.getUsername(), user.getPassword());
        List<UserDTO> users = userClient.getUsers(auth);
        Assert.assertEquals(users.size(),3);
        userClient.deleteUser(auth, user1.getId());
        userClient.deleteUser(auth, user2.getId());
        userClient.deleteUser(auth, user.getId());
    }
}
