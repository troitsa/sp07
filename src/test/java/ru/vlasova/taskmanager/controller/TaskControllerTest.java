package ru.vlasova.taskmanager.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.vlasova.taskmanager.api.service.ITaskService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.entity.Task;
import ru.vlasova.taskmanager.model.entity.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest extends AbstractController {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ITaskService taskService;

    @MockBean
    private IUserService userService;

    private Principal mockPrincipal = mock(Principal.class);

    private static User user;
    private Task task1 = newTask("");

    @Before
    public void setUp() {
        user = newUser("");
        userService.saveUser(user);
        when(mockPrincipal.getName()).thenReturn("test");
        when(userService.findByUsername("test")).thenReturn(user);
    }

    @After
    public void after() {
        userService.deleteUser(user.getId());
    }

    @Test
    @WithMockUser("test")
    public void taskList() throws Exception {
        List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        when(taskService.findAll())
                .thenReturn(tasks);
        mockMvc.perform(get("/task_list").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("task_list"))
                .andExpect(forwardedUrl("/view/task_list.jsp"))
                .andExpect(model().attribute("taskList", hasSize(1)))
                .andExpect(model().attribute("taskList", hasItem(
                        allOf(
                                hasProperty("id", is(task1.getId())),
                                hasProperty("description", is(task1.getDescription())),
                                hasProperty("name", is(task1.getName()))
                        )
                )));
    }

    @Test
    @WithMockUser("test")
    public void newTaskForm() throws Exception {
        mockMvc.perform(get("/new_task").principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("new_task"))
                .andExpect(forwardedUrl("/view/new_task.jsp"))
                .andExpect(model().attribute("task", hasProperty("id")));
    }

    @Test
    @WithMockUser("test")
    public void saveTask() throws Exception {
        mockMvc.perform(post("/task_save")
                .content(asJsonString(task1))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(view().name("redirect:/task_list"));
    }

    @Test
    @WithMockUser("test")
    public void editTaskForm() throws Exception {
        when(taskService.findOne(any(), any()))
                .thenReturn(task1);
        mockMvc.perform(get("/edit_task")
                .param("id", task1.getId())
                .principal(mockPrincipal))
                .andExpect(status().isOk())
                .andExpect(view().name("edit_task"))
                .andExpect(forwardedUrl("/view/edit_task.jsp"));
    }

    @Test
    @WithMockUser("test")
    public void deleteTaskForm() throws Exception {
        mockMvc.perform(post("/delete_task")
                .param("id", task1.getId())
                .principal(mockPrincipal))
                .andExpect(view().name("redirect:/task_list"));
    }

}
